;var KIDLY = {};
;(function($) {
    KIDLY.DEFAULTS = {
        appname:'Kidly',
        namespace:'KIDLY',
        success_message:'Welcome back! Redirecting...',
        redirect_url:'https://google.co.uk'
    };

    KIDLY.VALIDATOR = {
        init:function() {
            // alias required to 'email_required' with new message
            $.validator.addMethod("email_required", $.validator.methods.required,"Email can't be empty");
            // alias required to 'pass_required' with new message
            $.validator.addMethod("pass_required", $.validator.methods.required,"Password can't be empty");
            // add custom class rules
            $.validator.addClassRules("login_email", {email_required:true,email:true});
            $.validator.addClassRules("login_pass", {pass_required:true});

            $(document).submit('form#Login',function(){
                // validate login form on submit
                KIDLY.VALIDATOR.check($('form#Login'));
                // prevent form from submitting
                return false;
            });
        },
        check:function(form) {
            if($(form).valid()) {
                // show loading animation
                $('[type="submit"]').after('<div class="ButtonAnimation"></div>');
                // log in
                KIDLY.LOGIN.authorise();
            }
        }
    };

    KIDLY.LOGIN = {
        authorise:function(){
            setTimeout(function(){
                // remove loading animation
                $('.ButtonAnimation').remove();
                // display success message
                $('.SuccessMessage')
                    .html(KIDLY.DEFAULTS.success_message)
                    .fadeIn(500);
                // redirect to Google
                window.location.replace(KIDLY.DEFAULTS.redirect_url);
            },1500);
        },
        logout:function(){
            // nothing here
            $.noop();
        }
    }
})(jQuery);
KIDLY.VALIDATOR.init();